package com.example.servicebroker;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootTest
@EnableGlobalMethodSecurity
class ServicebrokerApplicationTests {

    @Test
    void contextLoads() {
    }

}
