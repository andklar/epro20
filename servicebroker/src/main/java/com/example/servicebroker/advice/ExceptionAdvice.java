package com.example.servicebroker.advice;

import com.example.servicebroker.controller.responses.BasicResponse;
import com.example.servicebroker.controller.ServiceInstanceController;
import com.example.servicebroker.exceptions.*;
import lombok.Data;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.lang.reflect.MalformedParametersException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceInstanceController.class);

    @ExceptionHandler(value = {ValidationException.class, IllegalNullException.class,
            NoSuchServiceDefinitionException.class, MalformedParametersException.class,
            NotMatchingParameterException.class})
    @ResponseBody
    public ResponseEntity<String> handleValidationException(Exception ex) {
        return new ResponseEntity<>(jsonFromException(ex), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value
            = {HeaderValidationException.class})
    protected ResponseEntity<Object> handleValidationConflict(
            RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(Collections.singletonList(ex.getMessage())),
                new HttpHeaders(), HttpStatus.PRECONDITION_FAILED, request);
    }

    @ExceptionHandler(value
            = {NoSuchElementException.class})
    protected ResponseEntity<Object> handleNoSucheElementConflict(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(Collections.singletonList(ex.getMessage())),
                new HttpHeaders(), HttpStatus.GONE, request);
    }


    @ExceptionHandler(value
            = {HttpClientErrorException.class})
    protected ResponseEntity<Object> handleHttpClientConflict(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(Collections.singletonList(ex.getMessage())),
                new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }


    @ExceptionHandler(value = {UnknownMaintenanceException.class})
    @ResponseBody
    public ResponseEntity<String> handleMaintenanceException(Exception ex) {
        return new BasicResponse<>(jsonFromException(ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(value
            = {Exception.class})
    protected ResponseEntity<Object> handleUnknownConflict(RuntimeException ex, WebRequest request) {
        LOG.error("Unhandled Exception occurred" + Arrays.toString(ex.getStackTrace()));
        return handleExceptionInternal(ex, new ErrorMessage(Collections.singletonList(ex.getMessage())),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @Data
    private static class ErrorMessage {
        private final String cause = "VALIDATION FAILED";
        private List<String> description;

        public ErrorMessage(List<String> description) {
            this.description = description;
        }
    }

    private String jsonFromException(Exception ex) {
        JSONObject response = new JSONObject();
        response.put("message", ex.getMessage());
        return response.toJSONString();
    }
}