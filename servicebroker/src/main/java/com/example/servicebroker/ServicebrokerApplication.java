package com.example.servicebroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicebrokerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicebrokerApplication.class, args);
    }

}
