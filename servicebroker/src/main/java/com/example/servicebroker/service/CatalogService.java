package com.example.servicebroker.service;

import com.example.servicebroker.model.Catalog;
import com.example.servicebroker.model.MaintenanceInfo;
import com.example.servicebroker.model.Plan;
import com.example.servicebroker.model.ServiceDefinition;
import com.example.servicebroker.repository.ServiceDefinitionRepository;
import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;


@Service
public class CatalogService {

    private final ServiceDefinitionRepository repository;

    public CatalogService(ServiceDefinitionRepository repository) {
        this.repository = repository;
    }

    public Catalog getCatalog() {
        return new Catalog(repository.findAll());
    }

    /**
     * Fills the catalog with some definitions
     */
    @PostConstruct
    public void createDummyDefinitions(){
        ServiceDefinition s = new ServiceDefinition();
        s.setName("test-a");
        s.setDescription("A test service definition");
        s.setBindable(true);
        s.setMaintenanceInfo(new MaintenanceInfo("1.0"));
        s.setPlans(Arrays.asList(
                new Plan(RandomString.make(),"small", "The smallest plan"),
                        new Plan(RandomString.make(),"medium", "The mediumest plan"),
                        new Plan(RandomString.make(),"big", "The biggest plan"))
        );
        repository.save(s);

        ServiceDefinition s2 = new ServiceDefinition();
        s2.setName("test-b");
        s2.setDescription("A test service definition");
        s2.setBindable(true);
        s2.setMaintenanceInfo(new MaintenanceInfo("2.1"));
        s2.setPlans(Arrays.asList(
                new Plan(RandomString.make(),"small", "The smallest plan"),
                new Plan(RandomString.make(),"medium", "The mediumest plan"),
                new Plan(RandomString.make(),"big", "The biggest plan"))
        );
        repository.save(s2);
    }
}
