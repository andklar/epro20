package com.example.servicebroker.service;

import com.example.servicebroker.controller.responses.InstanceResponse;
import com.example.servicebroker.controller.requests.ProvisioningRequest;
import com.example.servicebroker.controller.ServiceInstanceController;
import com.example.servicebroker.exceptions.NoSuchServiceDefinitionException;
import com.example.servicebroker.exceptions.NotMatchingParameterException;
import com.example.servicebroker.model.*;
import com.example.servicebroker.repository.ServiceInstanceRepository;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.Base64;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ServiceInstanceService {
    private final ServiceInstanceRepository repository;
    private Catalog catalog;

    private static final Logger LOG = LoggerFactory.getLogger(ServiceInstanceController.class);

    public ServiceInstanceService(ServiceInstanceRepository repository) {
        this.repository = repository;
    }

    /**
     * Retrieves an instance for the given instanceId
     * @param instanceId the id that shall be searched for
     * @return the service binding belonging to the id
     * @throws NoSuchElementException gets thrown if no instance with this id is known
     */
    public ServiceInstance getServiceInstance(String instanceId) throws NoSuchElementException{
        Optional<ServiceInstance> posInstance = repository.findById(instanceId);
        if (posInstance.isPresent()) {
            return posInstance.get();
        } else {
            throw new NoSuchElementException("No elemement for id: " + instanceId);
        }
    }

    /**
     * Retrieves the last known stat of the submitted instance
     * @param instanceId the instanceId to get the last state from
     * @param serviceId the matching service id
     * @param planId the matching service plan id
     * @return the last known state of the instance
     * @throws NotMatchingParameterException gets thrown when serviceId or planId don't match the instance
     */
    public String getLastOperationState(@NotNull String instanceId, String serviceId, String planId) throws NotMatchingParameterException {
        Optional<ServiceInstance> instance = repository.findById(instanceId);
        if (instance.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            if (!instance.get().getServiceId().equals(serviceId)) {
                throw new NotMatchingParameterException("service_id: " + serviceId + " doesn't match the submitted service_id: " + serviceId);
            }
            if (!instance.get().getPlanID().equals(planId)) {
                throw new NotMatchingParameterException("plan_id: " + planId + " doesn't match the submitted plan_id: " + serviceId);
            }
        }
        return instance.get().getLastOperation();
    }

    /**
     * Provisions a new service instance for the given request
     * @param instanceId the id that the new instance will get
     * @param request contains the data necessary to provision the new instance
     * @param lastOperation the operation that the instance shall initially have
     * @return the response with dashboard url and the last operation
     * @throws NoSuchServiceDefinitionException gets thrown when serviceId is unknown or the plan is unknown for the service
     */
    public InstanceResponse provisionServiceInstance(String instanceId, ProvisioningRequest request, String lastOperation) throws NoSuchServiceDefinitionException {
        updateCatalog();
        if (!catalog.contains(request.getServiceId())) {
            throw new NoSuchServiceDefinitionException("The provided service_id: " + request.getServiceId() + " is unknown.");
        }
        if (!catalog.contains(request.getServiceId(), request.getPlanId())) {
            throw new NoSuchServiceDefinitionException("The provided plan_id: " + request.getPlanId() + " is unknown for service_id: " + request.getServiceId());
        }
        ServiceInstance instance = new ServiceInstance();
        instance.setId(instanceId);
        instance.setPlanID(request.getPlanId());
        instance.setServiceId(request.getServiceId());
        instance.setSpaceGuid(request.getSpaceGuid());
        instance.setOrganizationGuid(request.getOrganizationGuid());
        instance.setLastOperation(lastOperation);
        repository.save(instance);
        return new InstanceResponse(instanceId, instance.getLastOperation());
    }

    /**
     * Provisions a new service instance for the given request
     * @param instanceId the id that the new instance will get
     * @param request contains the data necessary to provision the new instance
     * @return the response with dashboard url and the last operation "created"
     * @throws NoSuchServiceDefinitionException gets thrown when serviceId is unknown or the plan is unknown for the service
     */
    public InstanceResponse provisionServiceInstance(String instanceId, ProvisioningRequest request) throws NoSuchServiceDefinitionException {
        return provisionServiceInstance(instanceId,request,"created");
    }

    /**
     * Updates the internal catalog
     */
    private void updateCatalog() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.add("Authorization", "Basic " + Base64.getEncoder().encodeToString("admin:password".getBytes()));

        headers.add("X-Broker-API-Version", "2.15");
        HttpEntity<?> entity = new HttpEntity<>(headers);
        String catalogUrl = "http://localhost:8080/v2/catalog";

        ResponseEntity<Catalog> response = restTemplate.exchange(
                catalogUrl, HttpMethod.GET, entity, Catalog.class);
        this.catalog = response.getBody();
    }

    /**
     * Checks if an instance with identical parameters exists
     * @param instanceId
     * @param serviceId
     * @param planId
     * @param spaceGuid
     * @param organizationGuid
     * @return if an equal instance already exists
     */
    public boolean identicalInstanceExists(String instanceId, String serviceId, String planId, String spaceGuid, String organizationGuid) {
        Optional<ServiceInstance> instance = repository.findById(instanceId);
        return instance.isPresent() && instance.get().getServiceId().equals(serviceId) && instance.get().getPlanID().equals(planId)
                && instance.get().getSpaceGuid().equals(spaceGuid) && instance.get().getOrganizationGuid().equals(organizationGuid);
    }

    /**
     * Checks if an instance with the same instanceId but different parameters already exists
     * @param instanceId
     * @param serviceId
     * @param planId
     * @param spaceGuid
     * @param organizationGuid
     * @return if one of the parameters is conflicting
     */
    public boolean isConflicting(String instanceId, String serviceId, String planId, String spaceGuid, String organizationGuid) {
        Optional<ServiceInstance> instance = repository.findById(instanceId);
        return instance.isPresent() && (!instance.get().getServiceId().equals(serviceId) || !instance.get().getPlanID().equals(planId)
                || !instance.get().getSpaceGuid().equals(spaceGuid) || !instance.get().getOrganizationGuid().equals(organizationGuid));
    }

    /**
     * Fills some dummy instances in the service
     */
    @PostConstruct
    public void createDummyInstances() {
        ServiceInstance s = new ServiceInstance();
        s.setId(RandomString.make());
        s.setLastOperation("idle");
        s.setPlanID("1");
        s.setServiceId("1");
        repository.save(s);

        LOG.info("ServiceInstance created with " + s.getId());

        ServiceInstance s2 = new ServiceInstance();
        s2.setId(RandomString.make());
        s2.setLastOperation("idle");
        s2.setPlanID("1");
        s2.setServiceId("1");
        repository.save(s2);

        LOG.info("ServiceInstance created with " + s2.getId());
    }

    /**
     * Deprovisions the instance associated with the submitted instanceId
     * @param instanceId the id of the instance that gets deprovisioned
     * @param serviceId the matching serviceId of the instance
     * @param planId the matching planId of the instance
     * @throws NotMatchingParameterException gets thrown when either serviceId or planId don't match the instance
     */
    public void deprovisionInstance(@NotNull String instanceId, String serviceId, String planId) throws NotMatchingParameterException {
        Optional<ServiceInstance> instance = repository.findById(instanceId);
        if (instance.isPresent()) {
            if (!instance.get().getServiceId().equals(serviceId)) {
                throw new NotMatchingParameterException("service_id: " + serviceId + " doesn't match the submitted service_id: " + serviceId);
            }
            if (!instance.get().getPlanID().equals(planId)) {
                throw new NotMatchingParameterException("plan_id: " + planId + " doesn't match the submitted plan_id: " + serviceId);
            }
            repository.deleteById(instanceId);
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * Checks if the submitted ProvisioningRequest matches the definition in the catalog.
     * @param request the checked request
     * @return if the request matches the definition
     * @throws NotMatchingParameterException gets thrown if the submitted service definition is unknown
     */
    public boolean maintenanceMatchesService(ProvisioningRequest request) throws NotMatchingParameterException {
        if (request.getMaintenanceInfo() == null)
            return true;
        ServiceDefinition serviceDefinition = null;
        updateCatalog();
        for (ServiceDefinition s : catalog.getServiceDefinitionList()) {
            if (s.getId().equals(request.getServiceId()))
                serviceDefinition = s;
        }
        if (serviceDefinition == null) {
            throw new NotMatchingParameterException("service_id: " + request.getServiceId() + " doesn't match the submitted service_id: " + request.getServiceId());
        }
        return request.getMaintenanceInfo().equals(serviceDefinition.getMaintenanceInfo());
    }
}