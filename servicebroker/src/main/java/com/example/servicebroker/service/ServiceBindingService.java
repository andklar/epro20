package com.example.servicebroker.service;

import com.example.servicebroker.exceptions.NotMatchingParameterException;
import com.example.servicebroker.controller.responses.BindingResponse;
import com.example.servicebroker.model.ServiceBinding;
import com.example.servicebroker.controller.requests.ServiceBindingRequest;
import com.example.servicebroker.repository.ServiceBindingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ServiceBindingService{

    private final ServiceBindingRepository repository;

    @Autowired
    public ServiceBindingService(ServiceBindingRepository repository) {
        this.repository = repository;
    }

    /**
     * Retrieves the last operation state from the repository for the given binding.
     * @param bindingId the binding that is requested
     * @param serviceId the service that the binding belongs to
     * @param planId the plan that the binding belongs to
     * @return the last operation of the binding
     * @throws NotMatchingParameterException gets thrown if serviceId or planId don't fit the binding
     */
    public String getLastOperationState(@NotNull String bindingId, String serviceId, String planId) throws NotMatchingParameterException {
        Optional<ServiceBinding> binding = repository.findById(bindingId);
        if (!binding.isPresent()) {
            throw new NoSuchElementException();
        } else {
            if (!binding.get().getServiceId().equals(serviceId)) {
                throw new NotMatchingParameterException("service_id: " + serviceId + " doesn't match the submitted service_id: " + serviceId);
            }
            if (!binding.get().getPlanID().equals(planId)) {
                throw new NotMatchingParameterException("plan_id: " + planId + " doesn't match the submitted plan_id: " + serviceId);
            }
        }
        return binding.get().getLastOperation();
    }

    /**
     * Retrieves a binding registered under the given id
     * @param bindingId bindingId to retrieve
     * @return the Binding
     */
    public BindingResponse getBinding(String bindingId) {
        Optional<ServiceBinding> binding = repository.findById(bindingId);
        return new BindingResponse(binding.get().getCredentials());
    }

    /**
     * Creates a new binding for the given bindingrequest
     * @param bindingId the id that the generated binding will receive
     * @param request the request with all necessary data to create the binding
     * @return the instantiated binding
     */
    public BindingResponse generateServiceBinding(String bindingId, ServiceBindingRequest request) {
        ServiceBinding binding = new ServiceBinding();
        binding.setId(bindingId);
        binding.setPlanID(request.getPlanId());
        binding.setServiceId(request.getServiceId());
        binding.setLastOperation("created");
        binding.getCredentials().put("username", "admin");
        binding.getCredentials().put("password", "admin");
        repository.save(binding);
        return new BindingResponse(binding.getCredentials());
    }

    /**
     * Checks if a binding with the given parameters already exists
     * @param bindingId the id of the binding
     * @param serviceId the id of the service that has to match
     * @param planId the id of the service plan that has to match
     * @return if the parameters are already registered
     */
    public boolean identicalBindingExists(String bindingId, String serviceId, String planId) {
        Optional<ServiceBinding> binding = repository.findById(bindingId);
        return binding.isPresent() && binding.get().getServiceId().equals(serviceId) && binding.get().getPlanID().equals(planId);
    }

    /**
     * Checks if a binding with the given bindingId, but with different service or plan already exists
     * @param bindingId the id of the binding
     * @param serviceId the id of the service that has to match
     * @param planId the id of the service plan that has to match
     * @return if the binding already exists with at least one parameter not matching
     */
    public boolean isConflicting(String bindingId, String serviceId, String planId) {
        Optional<ServiceBinding> binding = repository.findById(bindingId);
        return binding.isPresent() && (!binding.get().getServiceId().equals(serviceId) || !binding.get().getPlanID().equals(planId));
    }

    /**
     * Deprovisions a binding
     * @param bindingId the binding to be decommissioned
     * @param serviceId the matching service id
     * @param planId the matching planId
     * @throws NotMatchingParameterException is thrown when serviceId or planId doesn't match the requested binding
     */
    public void deprovisionServiceBinding(String bindingId, String serviceId, String planId) throws NotMatchingParameterException {
        Optional<ServiceBinding> instance = repository.findById(bindingId);
        if (instance.isPresent()) {
            if (!instance.get().getServiceId().equals(serviceId)) {
                throw new NotMatchingParameterException("service_id: " + serviceId + " doesn't match the submitted service_id: " + serviceId);
            }
            if (!instance.get().getPlanID().equals(planId)) {
                throw new NotMatchingParameterException("plan_id: " + planId + " doesn't match the submitted plan_id: " + serviceId);
            }
            repository.deleteById(bindingId);
        } else {
            throw new NoSuchElementException("Element does not exist");
        }
    }

    /**
     * Checks if the a binding with the given id already exists
     * @param bindingId the id to be checked
     * @return if the binding exists
     */
    public boolean bindingExists(String bindingId) {
        return repository.existsById(bindingId);
    }
}