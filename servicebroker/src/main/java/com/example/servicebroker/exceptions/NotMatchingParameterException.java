package com.example.servicebroker.exceptions;

public class NotMatchingParameterException extends Exception {
    public NotMatchingParameterException(String s){
        super(s);
    }
}
