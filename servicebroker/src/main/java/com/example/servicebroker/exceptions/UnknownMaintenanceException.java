package com.example.servicebroker.exceptions;

public class UnknownMaintenanceException extends Exception{
    public UnknownMaintenanceException(String s){
        super(s);
    }
}
