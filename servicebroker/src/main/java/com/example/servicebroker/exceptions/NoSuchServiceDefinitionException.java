package com.example.servicebroker.exceptions;

public class NoSuchServiceDefinitionException extends Exception {
    public NoSuchServiceDefinitionException(String s) {
        super(s);
    }
}
