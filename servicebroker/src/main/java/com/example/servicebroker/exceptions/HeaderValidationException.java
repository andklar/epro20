package com.example.servicebroker.exceptions;

import javax.validation.ValidationException;

public class HeaderValidationException extends ValidationException {
    public HeaderValidationException(String errorMessage) {
        super(errorMessage);
    }
}
