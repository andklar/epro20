package com.example.servicebroker.exceptions;

public class IllegalNullException extends Exception {
    public IllegalNullException(String parameter) {
        super(parameter);
    }
}
