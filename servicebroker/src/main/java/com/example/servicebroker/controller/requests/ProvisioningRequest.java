package com.example.servicebroker.controller.requests;

import com.example.servicebroker.model.MaintenanceInfo;
import com.example.servicebroker.utils.ParameterValidator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ProvisioningRequest {

    @NotNull(message = "You have to provide a service_id")
    @Pattern(regexp = ParameterValidator.RFC3986_REGEX)
    @JsonSerialize
    @JsonProperty("service_id")
    private String serviceId;

    @NotNull(message = "You have to provide a plan_id")
    @Pattern(regexp = ParameterValidator.RFC3986_REGEX)
    @JsonSerialize
    @JsonProperty("plan_id")
    private String planId;

    @NotNull(message = "You have to provide a organization_guid")
    @JsonSerialize
    @JsonProperty("organization_guid")
    private String organizationGuid;

    @NotNull(message = "You have to provide a space_guid")
    @JsonSerialize
    @JsonProperty("space_guid")
    private String spaceGuid;

    @JsonProperty("maintenance_info")
    @JsonSerialize
    private MaintenanceInfo maintenanceInfo;

}
