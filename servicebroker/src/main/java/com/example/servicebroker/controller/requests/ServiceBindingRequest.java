package com.example.servicebroker.controller.requests;

import com.example.servicebroker.utils.ParameterValidator;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@JsonAutoDetect(getterVisibility = Visibility.NONE)
@Data
public class ServiceBindingRequest {

    @NotNull(message = "You have to provide a service_id")
    @Pattern(regexp = ParameterValidator.RFC3986_REGEX)
    @JsonSerialize
    @JsonProperty("service_id")
    private String serviceId;

    @NotNull(message = "You have to provide a plan_id")
    @Pattern(regexp = ParameterValidator.RFC3986_REGEX)
    @JsonSerialize
    @JsonProperty("plan_id")
    private String planId;
}
