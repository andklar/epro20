package com.example.servicebroker.controller.responses;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonAutoDetect(getterVisibility = Visibility.NONE)
public class BindingResponse implements GenericResponse{
    @JsonProperty("credentials")
    private Map<String, String> credentials;

    public BindingResponse(Map<String, String> credentials) { this.credentials = credentials; }

    public Map<String, String> getCredentials() {
        return this.credentials;
    }

    public void setCredentials(Map<String, String> credentials) {
        this.credentials = credentials;
    }
}
