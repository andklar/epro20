package com.example.servicebroker.controller;

import com.example.servicebroker.controller.requests.ServiceBindingRequest;
import com.example.servicebroker.controller.responses.BasicResponse;
import com.example.servicebroker.controller.responses.GenericResponse;
import com.example.servicebroker.exceptions.IllegalNullException;
import com.example.servicebroker.exceptions.NotMatchingParameterException;
import com.example.servicebroker.exceptions.ValidationException;
import com.example.servicebroker.service.ServiceBindingService;
import com.example.servicebroker.utils.HeaderValidator;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

@RestController
@EnableAutoConfiguration
public class ServiceBindingController {

    final private HeaderValidator headerValidator;

    private static final String PATH_MAPPING = "/v2/service_instances/{instanceId}/service_bindings/{bindingId}";

    private final ServiceBindingService service;

    public ServiceBindingController(ServiceBindingService serviceBindingService, HeaderValidator headerValidator) {
        this.service = serviceBindingService;
        this.headerValidator = headerValidator;
    }

    /**
     * GET
     * ​/v2​/service_instances​/{instanceId}​/service_bindings​/{bindingId}​/last_operation
     * get the last requested operation state for service binding
     *
     * @return the last_operation for the given binding
     */
    @GetMapping({PATH_MAPPING + "/last_operation"})
    public BasicResponse<String> getLastOperationState(@RequestHeader Map<String, String> headers,
                                               @PathVariable String bindingId, @PathVariable String instanceId,
                                               @RequestParam("service_id") String serviceId, @RequestParam("plan_id") String planId) throws IllegalNullException, NotMatchingParameterException {
        if(instanceId == null){
            throw new IllegalNullException("instance_id");
        }
        if(bindingId == null){
            throw new IllegalNullException("binding_id");
        }
        headerValidator.validateHeader(headers);

        return new BasicResponse<>(service.getLastOperationState(bindingId, serviceId, planId), HttpStatus.OK);
    }


    /**
     * PUT
     * ​/v2​/service_instances​/{instanceId}​/service_bindings​/{bindingId}
     * generate a service binding
     *
     * @return the generated service binding
     */
    @PutMapping({PATH_MAPPING})
    public BasicResponse<GenericResponse> generateServiceBinding(@RequestBody ServiceBindingRequest request,
                                                                 @PathVariable @NotNull String instanceId,
                                                                 @PathVariable @NotNull String bindingId) throws IllegalNullException, ValidationException {
        if(instanceId == null){
            throw new IllegalNullException("instance_id");
        }
        if(bindingId == null){
            throw new IllegalNullException("binding_id");
        }

        Set<ConstraintViolation<ServiceBindingRequest>> validate = Validation.buildDefaultValidatorFactory().getValidator().validate(request);
        if (!validate.isEmpty()) {
            throw new ValidationException(validate);
        }

        if(service.identicalBindingExists(bindingId, request.getServiceId(), request.getPlanId())) {
            // 200 OK when identical to existing binding
            return new BasicResponse<>(service.getBinding(bindingId), HttpStatus.OK);
        }
        else if(service.isConflicting(bindingId, request.getServiceId(), request.getPlanId())) {
            // 409 Conflict when binding already exists with different attributes
            return new BasicResponse<>(HttpStatus.CONFLICT);
        }
        else {
            // 201 Created if created successfully
            return new BasicResponse<>(service.generateServiceBinding(bindingId, request), HttpStatus.CREATED);
        }

    }

    /**
     * DELETE
     * ​/v2​/service_instances​/{instanceId}​/service_bindings​/{bindingId}
     * deprovision a service binding
     *
     * @return empty JSON if successful
     */
    @DeleteMapping({PATH_MAPPING})
    public BasicResponse<GenericResponse> deprovisionServiceBinding(@RequestParam("service_id") String serviceId,
                                                                    @RequestParam("plan_id") String planId,
                                                                    @PathVariable @NotNull String instanceId,
                                                                    @PathVariable @NotNull String bindingId) throws IllegalNullException, NotMatchingParameterException {

        if(instanceId == null){
            throw new IllegalNullException("instance_id");
        }
        if(bindingId == null){
            throw new IllegalNullException("binding_id");
        }

        if(!service.bindingExists(bindingId)) {
            // 410
            return new BasicResponse<>(HttpStatus.GONE);
        }
        else {

            // 200
            service.deprovisionServiceBinding(bindingId, serviceId, planId);
            return new BasicResponse<>(HttpStatus.OK);
        }
    }

    /**
     * GET
     * ​/v2​/service_instances​/{instanceId}​/service_bindings​/{bindingId}
     * get a service binding
     *
     * @return a service binding with given binding_id
     */
    @GetMapping({PATH_MAPPING})
    public BasicResponse<GenericResponse> getServiceBinding(@RequestHeader Map<String, String> headers,
                                           @PathVariable @NotNull String bindingId, @PathVariable @NotNull String instanceId) throws IllegalNullException {
        if(instanceId == null){
            throw new IllegalNullException("instance_id");
        }
        if(bindingId == null){
            throw new IllegalNullException("binding_id");
        }
        headerValidator.validateHeader(headers);
        return null;
    }
}
