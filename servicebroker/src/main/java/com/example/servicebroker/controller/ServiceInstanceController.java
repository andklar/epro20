package com.example.servicebroker.controller;

import com.example.servicebroker.controller.requests.ProvisioningRequest;
import com.example.servicebroker.controller.responses.BasicResponse;
import com.example.servicebroker.controller.responses.ErrorResponse;
import com.example.servicebroker.controller.responses.GenericResponse;
import com.example.servicebroker.controller.responses.InstanceResponse;
import com.example.servicebroker.exceptions.IllegalNullException;
import com.example.servicebroker.exceptions.NoSuchServiceDefinitionException;
import com.example.servicebroker.exceptions.NotMatchingParameterException;
import com.example.servicebroker.exceptions.ValidationException;
import com.example.servicebroker.model.*;
import com.example.servicebroker.service.ServiceInstanceService;
import com.example.servicebroker.utils.ParameterValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

@RestController
@EnableAutoConfiguration
public class ServiceInstanceController {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceInstanceController.class);

    private static final String PATH_MAPPING = "/v2/service_instances/{instanceId}";

    private final ServiceInstanceService service;

    /**
     * Construct a new {@link ServiceInstanceController}
     *
     * @param serviceInstanceService the service instance service
     */
    public ServiceInstanceController(ServiceInstanceService serviceInstanceService) {
        this.service = serviceInstanceService;
        LOG.info("ServiceInstanceController created");
    }


    /**
     * GET
     * ​/v2​/service_instances​/{instanceId}
     * get a service instance
     *
     * @return the instance for the given instance_id
     */
    @GetMapping({PATH_MAPPING})
    public BasicResponse<ServiceInstance> getServiceInstance(@PathVariable @NotNull String instanceId) throws IllegalNullException {
        if (instanceId == null || instanceId.isEmpty()) {
            throw new IllegalNullException("instance_id");
        }
        LOG.info("Correct call for GET instance");
        return new BasicResponse<>(service.getServiceInstance(instanceId), HttpStatus.OK);
    }


    /**
     * GET
     * ​/v2​/service_instances​/{instanceId}​/last_operation
     * get the last requested operation state for service instance
     *
     * @return the last_operation on the given instance_id
     */
    @GetMapping({PATH_MAPPING + "/last_operation"})
    public BasicResponse<String> getLastOperationState(@PathVariable @NotNull String instanceId,
                                                       @RequestBody(required = false) Map<String, String> body) throws IllegalNullException, NotMatchingParameterException {

        if (body == null) {
            throw new IllegalNullException("a body with service_id and plan_id must be submitted");
        }
        body.forEach((parameterValue, parameterName) -> ParameterValidator.validateParameter(parameterName));
        if (instanceId == null || instanceId.isEmpty()) {
            throw new IllegalNullException("instance_id");
        }
        LOG.info("Correct call for GET last operation (instance)");
        return new BasicResponse<>(service.getLastOperationState(instanceId, body.get("service_id"), body.get("plan_id")), HttpStatus.OK);
    }


    /**
     * PUT
     * ​/v2​/service_instances​/{instanceId}
     * provision a service instance
     *
     * @return the newly provisioned service instance
     */
    @PutMapping(PATH_MAPPING)
    public BasicResponse<GenericResponse> provisionServiceInstance(@PathVariable @NotNull String instanceId,
                                                                   @RequestBody ProvisioningRequest request
    ) throws Exception {
        if (validateRequest(instanceId, request))
            return new BasicResponse<>(new ErrorResponse("MaintenanceInfoConflict"), HttpStatus.UNPROCESSABLE_ENTITY);
        if (service.identicalInstanceExists(instanceId, request.getServiceId(), request.getPlanId(), request.getSpaceGuid(), request.getOrganizationGuid())) {
            // 200 OK when identical to existing instance
            LOG.info("Correct call for Put instance. But was already there. Nothing created");
            return new BasicResponse<>(new InstanceResponse(instanceId, service.getLastOperationState(instanceId,
                    request.getServiceId(), request.getPlanId())), HttpStatus.OK);
        } else if (service.isConflicting(instanceId, request.getServiceId(), request.getPlanId(), request.getSpaceGuid(), request.getOrganizationGuid())) {
            // 409 Conflict instance already exists but with different attributes
            LOG.error("Conflict in Put instance. instance_id already in use.");
            return new BasicResponse<>(new ErrorResponse("Conflict"), HttpStatus.CONFLICT);
        } else {
            // 201 Created if was created
            LOG.info("Correct call for Put instance. New instance created");
            return new BasicResponse<>(service.provisionServiceInstance(instanceId, request),
                    HttpStatus.CREATED);
        }
    }

    /**
     * Validates the request and throws exceptions if it fails
     * @param instanceId the instanceId to get validated
     * @param request the request that has to match
     * @return if the request did match
     * @throws IllegalNullException gets thrown if the instanceId is not set
     * @throws ValidationException gets thrown if the validation fails
     * @throws NotMatchingParameterException gets thrown if the submitted parameters in the request don't match the acutal instance
     */
    private boolean validateRequest(@PathVariable @NotNull String instanceId, @RequestBody ProvisioningRequest request) throws IllegalNullException, ValidationException, NotMatchingParameterException {
        if (instanceId == null || instanceId.isEmpty()) {
            throw new IllegalNullException("instance_id");
        }
        Set<ConstraintViolation<ProvisioningRequest>> validate = Validation.buildDefaultValidatorFactory().getValidator().validate(request);
        if (!validate.isEmpty()) {
            throw new ValidationException(validate);
        }
        // false when maintenance info doesn't match
        return !service.maintenanceMatchesService(request);
    }


    /**
     * PATCH
     * ​/v2​/service_instances​/{instanceId}
     * update a service instance
     *
     * @return the updated service instance
     */
    @PatchMapping({PATH_MAPPING})
    public BasicResponse<GenericResponse> updateServiceInstance(@PathVariable @NotNull String instanceId, @RequestBody ProvisioningRequest request) throws IllegalNullException, ValidationException, NotMatchingParameterException, NoSuchServiceDefinitionException {
        if (validateRequest(instanceId, request))
            return new BasicResponse<>(new ErrorResponse("MaintenanceInfoConflict"), HttpStatus.UNPROCESSABLE_ENTITY);
        return new BasicResponse<>(service.provisionServiceInstance(instanceId, request, "updated"),
                HttpStatus.OK);
    }


    /**
     * DELETE
     * ​/v2​/service_instances​/{instanceId}
     * deprovision a service instance
     *
     * @return empty json if successful
     */
    @DeleteMapping({PATH_MAPPING})
    public BasicResponse<GenericResponse> deprovisionServiceInstance(@PathVariable @NotNull String instanceId,
                                                                     @RequestParam("plan_id") String planId,
                                                                     @RequestParam("service_id") String serviceId) throws IllegalNullException, NotMatchingParameterException {
        if (instanceId == null || instanceId.isEmpty()) {
            throw new IllegalNullException("instance_id");
        }
        if (planId == null || planId.isEmpty()) {
            throw new IllegalNullException("plan_id");
        }
        if (serviceId == null || serviceId.isEmpty()) {
            throw new IllegalNullException("service_id");
        }
        service.deprovisionInstance(instanceId, serviceId, planId);
        return null;
    }
}
