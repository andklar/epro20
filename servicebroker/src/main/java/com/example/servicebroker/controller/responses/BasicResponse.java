package com.example.servicebroker.controller.responses;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BasicResponse<T> extends ResponseEntity<T> {

    public BasicResponse(HttpStatus status) {
        super(status);
    }

    public BasicResponse(T body, HttpStatus status) {
        super(body, status);
    }

}
