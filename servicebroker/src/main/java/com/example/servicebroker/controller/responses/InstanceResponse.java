package com.example.servicebroker.controller.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InstanceResponse implements GenericResponse {
    private static String DASHBOARD_URL_HEAD = "http://example-dashboard.example.com/";

    @JsonProperty("dashboard_url")
    private String dashboardUrl;
    private String operation;

    public InstanceResponse(String serviceId, String lastOperation) {
        this.dashboardUrl = DASHBOARD_URL_HEAD + serviceId;
        this.operation = lastOperation;
    }
}
