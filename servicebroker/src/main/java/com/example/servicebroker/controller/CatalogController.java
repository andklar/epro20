package com.example.servicebroker.controller;


import com.example.servicebroker.controller.responses.BasicResponse;
import com.example.servicebroker.model.Catalog;
import com.example.servicebroker.service.CatalogService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class CatalogController {
    private static final String PATH_MAPPING = "/v2/catalog";
    private final CatalogService service;

    public CatalogController(CatalogService service) {
        this.service = service;
    }

    /**
     * GET
     * ​/v2​/catalog
     * get the catalog of services that the service broker offers
     *
     * @return the catalog with aöl available services
     */
    @GetMapping({PATH_MAPPING})
    public BasicResponse<Catalog> getCatalog() {
        Catalog catalog = service.getCatalog();
        return new BasicResponse<>(catalog, HttpStatus.OK);
    }


}


