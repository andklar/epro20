package com.example.servicebroker.controller.responses;

import lombok.Data;

@Data
public class ErrorResponse implements GenericResponse {
    String error;

    public ErrorResponse(String error) {
        this.error = error;
    }
}
