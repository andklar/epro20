package com.example.servicebroker.utils;

import com.example.servicebroker.exceptions.HeaderValidationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class HeaderValidator implements HandlerInterceptor {

    @Value("${osb.api.version}")
    private String apiVersion;

    private static String API_HEADER = "X-Broker-API-Version".toLowerCase();

    /**
     * Checks if the submitted api matches the current osb api version
     * @param api the submitted api
     */
    private void checkApi(String api) {
        if (api == null || !apiVersion.equals(api.toLowerCase())) {
            throw new HeaderValidationException("Invalid Api version");
        }
    }

    /**
     * Validates the headers, to make sure all required fields are correct.
     * Currently API version is checked.
     * @param headers the request headers that get validated.
     */
    public void validateHeader(Map<String, String> headers) {
        checkApi(headers.get(API_HEADER));
    }


    /**
     * Interceptor method, that catches a request just before it reaches the controller.
     * @param request the external request
     * @param response
     * @param handler
     * @return always true. Signals that request needs further processing.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        checkApi(request.getHeader(API_HEADER));
        return true;
    }
}
