package com.example.servicebroker.utils;

import java.lang.reflect.MalformedParametersException;

public class ParameterValidator {
    public final static String RFC3986_REGEX = "[a-z0-9A-Z.\\-_~]*";

    /***
     * Asserts that the parameterName is RF3986 conform.
     * @param parameterName the parameterName that gets validated.
     * @throws MalformedParametersException when the parameterName is not RF3986 conform
     */
    public static void validateParameter(String parameterName) throws MalformedParametersException{
        if(!parameterName.matches(RFC3986_REGEX)){
            throw new MalformedParametersException(parameterName);
        }
    }
}
