package com.example.servicebroker.model;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class ServiceDefinition implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    private String name;

    private String description;

    private boolean bindable;

    @OneToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private MaintenanceInfo maintenanceInfo;

    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List<Plan> plans = new ArrayList<>();

    public String getId(){
        return ""+this.id;
    }
}
