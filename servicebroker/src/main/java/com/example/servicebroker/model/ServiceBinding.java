package com.example.servicebroker.model;


import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@Entity
public class ServiceBinding implements Serializable {
    @Id
    private String id;

    private String serviceId;

    @ElementCollection
    private Map<String, String> credentials = new HashMap<>();

    private String planID;

    private String lastOperation;
}
