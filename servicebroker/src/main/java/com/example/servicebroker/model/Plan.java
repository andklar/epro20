package com.example.servicebroker.model;

import lombok.Data;
import net.bytebuddy.utility.RandomString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
public class Plan implements Serializable {
    @Id
    private String id;
    private String name;
    private String description;

    public Plan(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Plan(){
        this.id = RandomString.make();
    }


    public String getId() {
        return "" + this.id;
    }
}
