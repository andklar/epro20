package com.example.servicebroker.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
public class ServiceInstance implements Serializable {
    @Id
    @JsonProperty("instance_id")
    private String id;

    @JsonProperty("service_id")
    private String serviceId;

    @JsonProperty("plan_id")
    private String planID;

    @JsonProperty("last_operation")
    private String lastOperation;

    @JsonProperty("space_guid")
    private String spaceGuid;

    @JsonProperty("organization_guid")
    private String organizationGuid;
}
