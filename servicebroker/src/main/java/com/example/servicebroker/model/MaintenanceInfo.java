package com.example.servicebroker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class MaintenanceInfo {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @NotNull
    @JsonProperty("version")
    private String version;
    @JsonProperty("description")
    private String description;

    public MaintenanceInfo(String version) {
        this.version = version;
    }

    public boolean equals(MaintenanceInfo info) {
        if (this.version == null && info.version == null) {
            return true;
        } else if (this.version == null) {
            return false;
        } else {
            return this.version.equals(info.version);
        }
    }

    public MaintenanceInfo() {

    }
}
