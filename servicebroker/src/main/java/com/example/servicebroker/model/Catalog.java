package com.example.servicebroker.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Catalog implements Serializable {
    @JsonProperty("services")
    List<ServiceDefinition> serviceDefinitionList;

    public Catalog(List<ServiceDefinition> serviceDefinitions) {
        serviceDefinitionList = serviceDefinitions;
    }

    public Catalog(){} // Dont delete, it is necessary!

    public boolean contains(String serviceId, String planId) {
        return serviceDefinitionList.stream().anyMatch(def -> def.getId().equals(serviceId) &&
                def.getPlans().stream().anyMatch(plan -> plan.getId().equals(planId)));
    }

    public boolean contains(String serviceId) {
        return serviceDefinitionList.stream().anyMatch(def -> def.getId().equals(serviceId));
    }
}
