package com.example.servicebroker.repository;

import com.example.servicebroker.model.ServiceDefinition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceDefinitionRepository extends JpaRepository<ServiceDefinition, String> {
}
