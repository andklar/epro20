
package com.example.servicebroker.repository;


import com.example.servicebroker.model.ServiceBinding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceBindingRepository extends JpaRepository<ServiceBinding, String> {
}
